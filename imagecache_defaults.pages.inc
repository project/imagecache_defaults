<?php
/**
 * @file
 * Administrative page callbacks for the ImageCache Defaults module.
 */

/**
 * Build form options to choose a default image.
 *
 * @return array
 *   An associative array appropriate for generating radio buttons.
 */
function imagecache_defaults_default_uri_options() {
  $scan = _imagecache_defaults_get_default_image_candidates();
  $options = array();
  $i = 0;
  foreach ($scan as $file) {
    $uri = $file->uri;
    $options[$uri] = $i ? $uri : $uri . ' ' . t('(Default)');
    $i++;
  }

  return $options;
}

/**
 * ImageCache Defaults global settings page.
 *
 * @return array
 *   A Drupal system settings form to be rendered by drupal_get_form().
 */
function imagecache_defaults_settings() {
  $form = array();

  $form['imagecache_defaults_global'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#title' => t('Site-wide default image settings'),
  );

  $form['imagecache_defaults_global']['imagecache_defaults_broken_path_handling'] = array(
    '#type' => 'radios',
    '#title' => t('Default image replacement behaviour'),
    '#description' => t('How should ImageCache Defaults handle broken and corrupt image file paths?'),
    '#options' => array(
      IMAGECACHE_DEFAULTS_DISABLED => t("Don't remove or modify any image paths."),
      IMAGECACHE_DEFAULTS_REMOVE_ALL => t("<em>Remove</em> everything sent to Image that doesn't have a valid image path."),
      IMAGECACHE_DEFAULTS_REPLACE_ALL => t("<em>Replace</em> everything sent to Image that doesn't have a valid image path."),
      IMAGECACHE_DEFAULTS_REPLACE_VALID => t("<em>Replace valid but broken</em> paths. Remove images with blank or corrupt paths. (Recommended)"),
    ),
    '#default_value' => variable_get('imagecache_defaults_broken_path_handling', IMAGECACHE_DEFAULTS_REPLACE_VALID),
  );

  $default_uri_options = imagecache_defaults_default_uri_options();
  $form['imagecache_defaults_global']['imagecache_defaults_default_image_uri'] = array(
    '#type' => 'radios',
    '#title' => t('Select default image'),
    '#description' => t('Which image should ImageCache Defaults use as the default image? Only images with the filename "imagecache_defaults" in a Drupal files directory will appear as an option.'),
    '#options' => $default_uri_options,
    '#default_value' => variable_get('imagecache_defaults_default_image_uri', key($default_uri_options)),
  );

  $form['imagecache_defaults_global']['imagecache_defaults_default_image_uri_override'] = array(
    '#type' => 'textfield',
    '#title' => t('Default image override'),
    '#description' => t('Enter the URI to a default image to override the selection above.'),
    '#default_value' => variable_get('imagecache_defaults_default_image_uri_override', ''),
  );

  $form['imagecache_defaults_global']['imagecache_defaults_default_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Default image preprocess style'),
    '#description' => t("The default image will be modified by this style before the replaced image's styles are applied."),
    '#options' => image_style_options(FALSE),
    '#default_value' => variable_get('imagecache_defaults_default_image_style', 'imagecache_defaults_prepare_default_image'),
  );

  $form['imagecache_defaults_global']['imagecache_defaults_bypass_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable the ImageCache Defaults cache (Not recommended).'),
    '#description' => t('<strong>Warning:</strong> This will force ImageCache Defaults to re-validate and lookup the dimensions of every image on every page load. This is a performance hit that may be negligible or very expensive, depending on your server setup.'),
    '#default_value' => variable_get('imagecache_defaults_bypass_cache', FALSE),
  );

  $form['imagecache_defaults_reporting'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#title' => t('Broken image reports'),
  );

  $form['imagecache_defaults_reporting']['imagecache_defaults_watchdog_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log a watchdog message whenever ImageCache Defaults attempts to repair an image path.'),
    '#description' => t('<strong>Warning:</strong> This can easily result in many log entries for sites that have been recently migrated with an incomplete or missing files directory.'),
    '#default_value' => variable_get('imagecache_defaults_watchdog_enabled', FALSE),
  );

  // This is rendered by the page callback function.
  return system_settings_form($form);
}
